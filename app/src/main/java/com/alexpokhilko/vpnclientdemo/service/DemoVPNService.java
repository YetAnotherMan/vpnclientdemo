package com.alexpokhilko.vpnclientdemo.service;

import android.content.Intent;
import android.net.VpnService;
import android.os.Handler;
import android.os.ParcelFileDescriptor;
import android.widget.Toast;

import com.alexpokhilko.vpnclientdemo.utils.events.EventStopService;
import com.orhanobut.logger.Logger;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.net.InetSocketAddress;
import java.nio.channels.DatagramChannel;

/**
 * Created by Александр on 12.02.2017.
 */
public class DemoVPNService extends VpnService {

    public static final String EXTRA_ADDRESS = "extra_address";
    public static final String EXTRA_PORT = "extra_port";
    public static final String EXTRA_KEY = "extra_key";

    private String serverAddress;
    private String serverPort;
    private Thread thread;
    private ParcelFileDescriptor intrface;

    private Handler handler;

    @Override
    public void onCreate(){
        super.onCreate();
        EventBus.getDefault().register(this);
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        // Stop the previous session by interrupting the thread.
        if (thread != null) {
            thread.interrupt();
        }

        handler = new Handler();

        // Extract information from the intent.
        serverAddress = intent.getStringExtra(EXTRA_ADDRESS);
        serverPort = intent.getStringExtra(EXTRA_PORT);
        //sharedSecret = intent.getStringExtra(EXTRA_KEY).getBytes();

        // Start a new session by creating a new thread.
        thread = new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    //a. Configure the TUN and get the interface.
                    intrface = new Builder()
                            .setSession("VPNService")
                            .addAddress(serverAddress, 24)
                            .establish();
                    //Packets to be sent are queued in this input stream.
                    FileInputStream in = new FileInputStream(intrface.getFileDescriptor());
                    //Packets received need to be written to this output stream.
                    FileOutputStream out = new FileOutputStream(intrface.getFileDescriptor());
                    //The UDP channel can be used to pass/get ip package to/from server
                    DatagramChannel tunnel = DatagramChannel.open();
                    //Connect to the server, localhost is used for demonstration only.
                    tunnel.connect(new InetSocketAddress(serverAddress, Integer.parseInt(serverPort)));
                    //Protect this socket, so package send by it will not be feedback to the vpn service.
                    protect(tunnel.socket());

                    if( tunnel.isConnected() ){
                        showToast("Connected");
                    } else {
                        showToast("Not Connected");
                    }

                    //Use a loop to pass packets.
                    while (true) {
                        Logger.d("data loop");
                        //get packet with in
                        //put packet to tunnel
                        //get packet form tunnel
                        //return packet with out
                        //sleep is a must
                        Thread.sleep(100);
                    }
                } catch (Exception e) {
                    // Catch any exception
                    e.printStackTrace();
                } finally {
                    try {
                        if (intrface != null) {
                            intrface.close();
                            intrface = null;
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }

        }, "MyVpnRunnable");

        //start the service
        thread.start();

        return START_STICKY;
    }
    @Override
    public void onDestroy() {
        EventBus.getDefault().unregister(this);
        super.onDestroy();
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMessageEvent(EventStopService event) {
        if( thread != null ) {
            thread.interrupt();
            Logger.d("stop thread");
        }

        stopSelf();
    }

    public void showToast(final String msg){
        handler.post(new Runnable() {
            @Override
            public void run() {
                Toast.makeText(DemoVPNService.this, msg, Toast.LENGTH_SHORT).show();
            }
        });
    }
}