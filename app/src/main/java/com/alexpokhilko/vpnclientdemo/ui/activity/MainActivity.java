package com.alexpokhilko.vpnclientdemo.ui.activity;

import android.app.ActivityManager;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.net.VpnService;
import android.os.Bundle;
import android.os.Environment;
import android.support.v4.text.TextUtilsCompat;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.alexpokhilko.vpnclientdemo.service.DemoVPNService;
import com.alexpokhilko.vpnclientdemo.utils.events.EventStopService;
import com.alexpokhilko.vpnclientdemo.utils.Preferences;
import com.alexpokhilko.vpnclientdemo.R;
import com.nononsenseapps.filepicker.FilePickerActivity;
import com.orhanobut.logger.Logger;

import org.apache.commons.io.FileUtils;
import org.greenrobot.eventbus.EventBus;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.IOException;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class MainActivity extends AppCompatActivity {

    public static final int VPN_REQUEST_CODE = 13;
    public static final int FILE_CODE = 113;
    public static final int JSON_FILE_CODE = 1113;

    public static final String JSON_FIELD_ADDRESS = "address";
    public static final String JSON_FIELD_PORT = "port";

    public String ADDRESS = "37.187.152.49";
    public String PORT = "443"; //TCP port

    @BindView(R.id.tvServerInfo)
    TextView tvServerInfo;

    @BindView(R.id.tvKey)
    TextView tvKey;

    @BindView(R.id.btnStartStopService)
    Button btnStartStop;

    private Preferences prefs;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);

        prefs = Preferences.getInstance(this);
        tvKey.setText(prefs.getKey());
        showServerInfo();

        if( !isServiceRunning(DemoVPNService.class) ) {
            btnStartStop.setText(R.string.btn_start_service);
        } else {
            btnStartStop.setText(R.string.btn_stop_service);
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode,  Intent data){
        super.onActivityResult(requestCode, resultCode, data);

        if( requestCode == VPN_REQUEST_CODE && resultCode == RESULT_OK ) {
            Intent serviceIntent = new Intent(this, DemoVPNService.class);
            serviceIntent.putExtra(DemoVPNService.EXTRA_ADDRESS, ADDRESS);
            serviceIntent.putExtra(DemoVPNService.EXTRA_PORT, PORT);
            serviceIntent.putExtra(DemoVPNService.EXTRA_KEY, prefs.getKey());
            startService(serviceIntent);

            btnStartStop.setText(R.string.btn_stop_service);
        }

        //file chooser interaction
        if (requestCode == FILE_CODE && resultCode == RESULT_OK) {
            Uri uri = data.getData();
            File file = com.nononsenseapps.filepicker.Utils.getFileForUri(uri);
            try {
                String fileContent = FileUtils.readFileToString(file, "UTF-8");
                Logger.d("fileContent:"+fileContent);
                prefs.saveKey(fileContent);
                tvKey.setText(fileContent);
            } catch (IOException e) {
                e.printStackTrace();
                Logger.d(e.getMessage());
            }
        }

        if (requestCode == JSON_FILE_CODE && resultCode == RESULT_OK) {
            Uri uri = data.getData();
            File file = com.nononsenseapps.filepicker.Utils.getFileForUri(uri);
            try {
                String fileContent = FileUtils.readFileToString(file, "UTF-8");
                try {
                    JSONObject jsonObject = new JSONObject(fileContent);
                    if( jsonObject.has(JSON_FIELD_ADDRESS) ){
                        ADDRESS = jsonObject.getString(JSON_FIELD_ADDRESS);
                        prefs.saveAddress(ADDRESS);
                    }

                    if( jsonObject.has(JSON_FIELD_PORT) ){
                        PORT = jsonObject.getString(JSON_FIELD_PORT);
                        prefs.savePort(PORT);
                    }

                    showServerInfo();
                } catch (JSONException e) {
                    e.printStackTrace();
                    Toast.makeText(this, R.string.err_not_json_file, Toast.LENGTH_SHORT).show();
                }
            } catch (IOException e) {
                e.printStackTrace();
                Logger.d(e.getMessage());
            }
        }
    }

    @OnClick(R.id.btnLoadKey)
    public void btnLoadClick(){
        Intent i = new Intent(this, FilePickerActivity.class);
        i.putExtra(FilePickerActivity.EXTRA_ALLOW_MULTIPLE, false);
        i.putExtra(FilePickerActivity.EXTRA_ALLOW_CREATE_DIR, false);
        i.putExtra(FilePickerActivity.EXTRA_MODE, FilePickerActivity.MODE_FILE);

        // Configure initial directory by specifying a String.
        // You could specify a String like "/storage/emulated/0/", but that can
        // dangerous. Always use Android's API calls to get paths to the SD-card or
        // internal memory.
        i.putExtra(FilePickerActivity.EXTRA_START_PATH, Environment.getExternalStorageDirectory().getPath());

        startActivityForResult(i, FILE_CODE);
    }

    @OnClick(R.id.btnLoadConfig)
    public void btnLoadConfig(){
        Intent i = new Intent(this, FilePickerActivity.class);
        i.putExtra(FilePickerActivity.EXTRA_ALLOW_MULTIPLE, false);
        i.putExtra(FilePickerActivity.EXTRA_ALLOW_CREATE_DIR, false);
        i.putExtra(FilePickerActivity.EXTRA_MODE, FilePickerActivity.MODE_FILE);

        // Configure initial directory by specifying a String.
        // You could specify a String like "/storage/emulated/0/", but that can
        // dangerous. Always use Android's API calls to get paths to the SD-card or
        // internal memory.
        i.putExtra(FilePickerActivity.EXTRA_START_PATH, Environment.getExternalStorageDirectory().getPath());

        startActivityForResult(i, JSON_FILE_CODE);
    }

    @OnClick(R.id.btnStartStopService)
    public void btnStartStopService(){
        if( !isServiceRunning(DemoVPNService.class) ) {
            //request permissions
            Intent intent = VpnService.prepare(this);
            if (intent != null)
                startActivityForResult(intent, VPN_REQUEST_CODE);
            else
                onActivityResult(VPN_REQUEST_CODE, RESULT_OK, null);
        } else {
            btnStartStop.setText(R.string.btn_start_service);
            EventBus.getDefault().post(new EventStopService());
        }
    }

    private void showServerInfo(){
        if( !TextUtils.isEmpty(prefs.getAddress()) )
            ADDRESS = prefs.getAddress();

        if( !TextUtils.isEmpty(prefs.getPort()) )
            PORT = prefs.getPort();

        String log = String.format("Address: %s\nPort: %s\n", ADDRESS, PORT);
        tvServerInfo.setText(log);
    }

    private boolean isServiceRunning(Class<?> serviceClass) {
        ActivityManager manager = (ActivityManager) getSystemService(Context.ACTIVITY_SERVICE);
        for (ActivityManager.RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
            if (serviceClass.getName().equals(service.service.getClassName())) {
                return true;
            }
        }
        return false;
    }
}
