package com.alexpokhilko.vpnclientdemo.app;

import android.app.Application;

import com.orhanobut.logger.Logger;

/**
 * Created by Александр on 12.02.2017.
 */

public class TheApp extends Application {

    private static final String LOG_TAG = "VPNDemo";

    @Override
    public void onCreate(){
        Logger.init(LOG_TAG)                 // default PRETTYLOGGER or use just init()
                .methodCount(3)                 // default 2
                .hideThreadInfo()               // default shown
                .methodOffset(2);                // default 0
    }
}
