package com.alexpokhilko.vpnclientdemo.utils;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * Created by Александр on 12.02.2017.
 */

public class Preferences {

    private static final String PREF_NAME = "vpndemoprefs";

    private static final String EXTRA_KEY = "extra_key";
    private static final String EXTRA_ADDRESS = "extra_address";
    private static final String EXTRA_PORT = "extra_port";

    private SharedPreferences appPrefs;
    private SharedPreferences.Editor appPrefsEditor;

    private static Preferences instance;
    private Context context;

    public static Preferences getInstance(Context context){
        if( instance == null )
            instance = new Preferences(context);
        return instance;
    }

    private Preferences(Context context){
        this.context = context;

        appPrefs = context.getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE);
        appPrefsEditor = appPrefs.edit();
    }

    public void saveKey(String key){
        appPrefsEditor.putString(EXTRA_KEY, key).apply();
    }

    public String getKey(){
        return appPrefs.getString(EXTRA_KEY,"");
    }

    public void saveAddress(String addr){
        appPrefsEditor.putString(EXTRA_ADDRESS, addr).apply();
    }

    public String getAddress(){
        return appPrefs.getString(EXTRA_ADDRESS,"");
    }

    public void savePort(String port){
        appPrefsEditor.putString(EXTRA_PORT, port).apply();
    }

    public String getPort(){
        return appPrefs.getString(EXTRA_PORT,"");
    }

}